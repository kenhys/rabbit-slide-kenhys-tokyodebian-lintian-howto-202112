# lintianに\\n新規ルールを\\n追加する方法

subtitle
:  2.11X.0対応版

author
:  Kentaro Hayashi

institution
:  ClearCode Inc.

content-source
:  2021年12月 東京エリア・関西合同Debian勉強会

allotted-time
:  20m

theme
:  .

# スライドはRabbit Slide Showにて公開済みです

* lintianに新規ルールを追加する方法
  * <https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-lintian-howto-202112/>

# 本日の内容

* lintianについて
* lintianのしくみ
* ルールの追加方法解説
* 実際に追加しながら学ぶ

# lintianについて

* /lɪntən/ <https://wiki.debian.org/Lintian>
  * Debianパッケージをチェックするツール
    * Debianポリシー違反を検出
    * よくあるミスを検出 (+dfsgを+dsfgとtypoしているとか)
    * よいパッケージングのやりかたを示す

# lintianについて

* salsaでメンテナンスされている
  * <https://salsa.debian.org/lintian/lintian>
* Perlによる実装
* 実行例: lintian -EvIL +pedantic path/to/pkg_version_arch.changes

# lintianのしくみ

* パッケージをパース
* 各種検出ルールを適用する
* 問題のある箇所をtagとして検出
* 結果をまとめて出力する

# lintianの参考資料

* <https://wiki.debian.org/Teams/Lintian/HackersGuide>
  * これは古くてあまり参考にならない
* <https://salsa.debian.org/lintian/lintian/-/blob/master/CONTRIBUTING.md>
* <https://salsa.debian.org/lintian/lintian/-/blob/master/doc/README.developers>
  * perldoc README.developersすべし

# 注意事項

* lintian 2.11X.0をベースに説明します
  * 結構アクティブに開発され、変更が入るため

# lintianのディレクトリ構成

* 大事なのは以下の3つ
  * lib/ 検出するルールを実装
  * t/ テストを追加する
  * tags/ タグを定義する

# lintianのルール開発

* 検出するためのコードを書く
  * lib/Lintian/Check/*
* テストを追加する
  * t/recipes/checks/*
* tagを追加する
  * tags/x/xxx.tag

# lintianで検出したいルール

* 判断に迷いがちなやつは機械的に検出したい
  * 例: +dfsgか~dfsgどっちを使うか問題
  * 例: +dfsgのrepack countどうあるべきか問題

# +dfsgか~dfsgどっちを使うか問題

* +dfsgが主流 (出典: UDDのsidのsource package調べ)
  * +dfsg: 1974パッケージ
  * ~dfsg: 109パッケージ
    * 例: ghostscript 9.55.0~dfsg-3

# あえて~dfsgを使う理由

* upstreamが同一バージョンでリリースしなおした場合に便利
  * upstream: ライセンス問題あるやつを公開(1.0)した🔥
  * debian: 1.0~dfsg-1としてパッケージングした
  * upstream: 同じバージョンで再リリースした
  * debian: 1.0-1 > 1.0~dfsg-1 なので、1.0-1として再度パッケージングすればよい
    * +dfsg使うと1.0+dfsg-1 > 1.0-1なのでアップグレードできない

# 結論: +dfsgか~dfsgどっちを使うか問題

* +dfsgで実用上は十分
* +dfsgに移行を促していくのがよいのでは
  * debian-policyではなさそう
  * developers-referenceにあるとよい?
    * <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=499167>
  * lintianがinfoレベルとしてチェックしてくれると気づきやすい

# +dfsgのrepack countどうあるべきか問題

* repack countもバラバラ(出典: UDDのsid source package調べ)


|repack countの付け方|パッケージ数| 
|--|---:|
|+dfsg-1|~1400|
|+dfsgN-1|~400|
|+dfsg.N-1|~60|

* mentors.debian.netはなぜか+dfsg.N-1をオススメしている🤔

# 結論: repack countはどう付与するのがよいか?

* 初回は+dfsg-1スタイル
  * 理由: ほぼ複数回repackすることはない
* 再度repackしないといけなくなったら、+dfsgN-1
  * 例: 1.0+dfsg2-1
  * 1.0+dfsg2-1 > 1.0+dfsg-1なので更新も問題ない
* lintianがinfoレベルとしてチェックしてくれると気づきやすい

# 実践編

* ~dfsgをlintianでチェックできるようにする
  * 検出するためのコードを書く
    * lib/Lintian/Check/*
  * テストを追加する
    * t/recipes/checks/*
  * tagを追加する
    * tags/x/xxx.tag

# ~dfsgをチェックする

* lib/Lintian/Check/Fields/Version.pm

```
--- a/lib/Lintian/Check/Fields/Version.pm                                                                                                   
+++ b/lib/Lintian/Check/Fields/Version.pm                                                                                                   
...
     } elsif ($version =~ /\.dfsg/) {
         $self->hint('dfsg-version-with-period', $version);
+                                                                                                                                           
+    } elsif ($version =~ /~dfsg/) {                                                                                                        
+        $self->hint('dfsg-version-with-tilde', $version);                                                                                  
+                                                                                                                                           
     } elsif ($version =~ /dsfg/) {
         $self->hint('dfsg-version-misspelled', $version);
     }
...
```


# ~dfsgのテストを追加する

* 主に以下の3つを追加する
  * t/recipes/checks/fields/version/**tilde-dfsg/build-spec/fill-values**
  * t/recipes/checks/fields/version/**tilde-dfsg/eval/desc**
  * t/recipes/checks/fields/version/**tilde-dfsg/eval/hints**

# ~dfsgのテストを追加する

* t/.../tilde-dfsg/build-spec/fill-values
  * パラメータ指定でテスト用パッケージをカスタマイズする
* t/.../tilde-dfsg/eval/desc
  * テストがどのグループに属するかを指定する
* t/.../tilde-dfsg/eval/hints
  * lintianでチェックしたときのメッセージ(自動生成する)

# t/.../tilde-dfsg/build-spec/fill-valuesの例

```
Testname: tilde-dfsg
Skeleton: upload-non-native
Version: 1.0~dfsg-1
Description: Check for dfsg with tilde (~) prefix
See-Also:
 https://lists.debian.org/debian-devel/2021/10/msg00012.html
```

* Skeleton: で指定したパッケージの雛形を上書きする

# t/.../tilde-dfsg/eval/descの例

```
Testname: tilde-dfsg
Check: fields/version
```

* Testname: はevalの親ディレクトリ(fill-valuesと同じ)
* Check:はこのテストが所属するグループを指定する

# t/.../tilde-dfsg/eval/hintsの例


```
tilde-dfsg (source): dfsg-version-with-tilde 1.0~dfsg-1
```

* テスト実行時に自動生成する

# tags/d/dfsg-version-with-tilde.tagの例
                                                                                    
```
Tag: dfsg-version-with-tilde
Severity: info
Check: fields/version
Explanation: The source version string contains a tilde like <code>~dfsg</code>.
 It is probably in a form like <code>1.0~dfsg-1</code>.
 ...
See-Also:
 https://lists.debian.org/debian-devel/2021/10/msg00012.html
```

* タグ名、重要度など詳細を記述する

# ~dfsgのhintを生成する

* ./private/runtests --onlyrun=check:fields/version

```
Building the test packages took 1 second.

# Hints do not match
# 
# --- debian/test-out/eval/checks/fields/version/tilde-dfsg/hints.specified.calibrated
# +++ debian/test-out/eval/checks/fields/version/tilde-dfsg/hints.actual.parsed
# -
# +tilde-dfsg (source): dfsg-version-with-tilde 1.0~dfsg-1
# 
```

# テストがパスすることを確認する

* 開発時は`--onlyrun`で実行するテストのグループをしぼるのがよい

```
$ ./private/runtests --onlyrun=check:fields/version
...
All tests successful.
Files=4, Tests=4,  2 wallclock secs ( 0.41 usr  0.12 sys +  4.25 cusr  1.02 csys =  5.80 CPU)
Result: PASS
```

# さいごに

* lintian 2.113.0のリリースに取り込まれました
  * <https://salsa.debian.org/lintian/lintian/-/merge_requests/379> ~dfsgのチェック
  * <https://salsa.debian.org/lintian/lintian/-/merge_requests/381> +dfsg.N-1のチェック
  * <https://salsa.debian.org/lintian/lintian/-/merge_requests/380> +dfsg1-1のチェック
* けっこうあっさりマージしてもらえるのでフィードバックおすすめ
